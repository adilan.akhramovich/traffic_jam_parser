FROM python:3.11

WORKDIR /app

COPY requirements.txt .

RUN apt-get update && apt-get install -y \
    libpq-dev \
    wget \
    unzip \
    libnss3 \
    libgconf-2-4 \
    libx11-6 \
    libx11-xcb1 \
    libxcomposite1 \
    libxcursor1 \
    libxdamage1 \
    libxi6 \
    libxtst6 \
    libcups2 \
    libxrandr2 \
    libasound2 \
    libatk1.0-0 \
    libgtk-3-0 \
    libpangocairo-1.0-0 \
    libpango1.0-0 \
    libgdk-pixbuf2.0-0 \
    libgl1-mesa-glx \
    xvfb

# Install Chrome version 104.0.5112.102
RUN wget --no-verbose -O /tmp/chrome.deb https://www.slimjet.com/chrome/download-chrome.php?file=files%2F104.0.5112.102%2Fgoogle-chrome-stable_current_amd64.deb
RUN apt-get -y update
RUN apt-get install -y /tmp/chrome.deb

# Install Chromedriver version 114
RUN wget -O /tmp/chromedriver.zip https://chromedriver.storage.googleapis.com/104.0.5112.79/chromedriver_linux64.zip
RUN unzip /tmp/chromedriver.zip -d /usr/local/bin/
RUN chmod +x /usr/local/bin/chromedriver
RUN rm /tmp/chromedriver.zip

RUN pip install --no-cache-dir -r requirements.txt

COPY main.py .
COPY new_fragmentated_address_roads.geojson .

CMD [ "python", "-u", "main.py" ]
