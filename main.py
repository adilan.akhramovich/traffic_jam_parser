import io
import gc
import time
import queue
import schedule
import threading
import geopandas as gpd
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from shapely.geometry import LineString
from shapely.wkb import dumps

import psycopg2
from psycopg2 import sql
from sqlalchemy import create_engine
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

from PIL import Image, ImageDraw, ImageFont

result_queue = None

# Set the size of the window
WINDOW_WIDTH = 30720
WINDOW_HEIGHT = 1440

images = None
stitched_images = None

# Set colors
burgundy = (169,  39,  39) # tolerance=15
red      = (242,  78,  66) # tolerance=25
yellow   = (255, 207,  67) # tolerance=60
green    = ( 62, 219, 128) # tolerance=50

# Set Radius
R = 8

roads = gpd.read_file('new_fragmentated_address_roads.geojson')

# Set resolution to coordinate settings
y_range  = 24
y_delta  = 0.01125
y_pixels = WINDOW_HEIGHT

x_delta  = 0.32948692
x_pixels = WINDOW_WIDTH

# Right-Top Corner
# Longitude: 77.08485341
# Latitude:  43.44
RT_long = 77.08485341
RT_lat  = 43.44

# Left-Bottom Corner
# Longitude: 76.75536659
# Latitude:  43.17
LB_long = 76.75536659
LB_lat  = 43.17

ys = [round(LB_lat + y_delta * index + (y_delta / 2), 6) for index in range(y_range)]
x  = 76.92011000000001

def is_in_area(point):
    
    global LB_long
    global RT_long
    global LB_lat
    global RT_lat
    
    if ((point[0] >= LB_long) and (point[0] < RT_long)) and ((point[1] >= LB_lat) and (point[1] < RT_lat)):
        return True
    return False

def get_image_index_and_pixel(point):
    
    global R
    global LB_long
    global LB_lat
    global y_range
    global x_pixels
    global y_pixels
    global x_delta
    global y_delta
    
    index = int((point[1] - LB_lat) / y_delta)
    lb_lat = LB_lat + index * y_delta
    i_pixel = int(round((point[0] - LB_long) / x_delta * x_pixels))
    j_pixel = y_pixels - int((point[1] - lb_lat) / y_delta * y_pixels)
        
    if index < 23:
        j_pixel += R
            
    return index, i_pixel, j_pixel

def stitch_images(images, direction='vertical'):
    # Ensure all images have the same height (for horizontal stitching) or width (for vertical stitching)
    sizes = [img.size for img in images]
    if direction == 'horizontal':
        target_size = (sum(sizes[i][0] for i in range(len(sizes))), sizes[0][1])
    elif direction == 'vertical':
        target_size = (sizes[0][0], sum(sizes[i][1] for i in range(len(sizes))))
    else:
        raise ValueError("Invalid direction. Use 'horizontal' or 'vertical'.")

    # Create a blank canvas
    stitched_image = Image.new('RGB', target_size)

    # Paste each image onto the canvas
    offset = 0
    for img in images:
        if direction == 'horizontal':
            stitched_image.paste(img, (offset, 0))
            offset += img.width
        elif direction == 'vertical':
            stitched_image.paste(img, (0, offset))
            offset += img.height

    return stitched_image

def crop_image(image, y1, y2, x1, x2):
    
    return image.crop((x1, y1, x2, y2))

def glue_images(y, shift):

    global images
    global x_pixels
    global y_pixels
    
    vertical_images = []
    
    if y < 23:
        vertical_images.append(crop_image(images[y+1], y_pixels-shift, y_pixels, 0, x_pixels))

    vertical_images.append(crop_image(images[y], 0, y_pixels, 0, x_pixels))  

    if y > 0:
        vertical_images.append(crop_image(images[y-1], 0, shift, 0, x_pixels))
    
    return stitch_images(vertical_images, direction='vertical')

def get_pixels_colors(image, center, radius):
    width, height = image.size
    pixels_colors = []

    for x in range(max(0, center[0] - radius), min(width, center[0] + radius + 1)):
        for y in range(max(0, center[1] - radius), min(height, center[1] + radius + 1)):
            # Check if the pixel is within the circular region
            distance = ((x - center[0]) ** 2 + (y - center[1]) ** 2) ** 0.5
            if distance <= radius:
                # Get the color of the pixel
                pixel_color = image.getpixel((x, y))
                pixels_colors.append(pixel_color)

    return pixels_colors

def color_counter(pixels_colors, target_color, tolerance=0):

    # Define the tolerance range for color matching
    min_r, min_g, min_b = [c - tolerance for c in target_color]
    max_r, max_g, max_b = [c + tolerance for c in target_color]
    
    counter = 0

    # Replace the target color with the replacement color
    for pixel_color in pixels_colors:
        r, g, b = pixel_color

        # Check if the pixel color is within the tolerance range of the target color
        if min_r <= r <= max_r and min_g <= g <= max_g and min_b <= b <= max_b:
            counter += 1

    # return the modified image
    return counter

def get_point_color(point, R):

    global stitched_images
    
    burgundy = (169,  39,  39) # tolerance=15
    red      = (242,  78,  66) # tolerance=25
    yellow   = (255, 207,  67) # tolerance=60
    green    = ( 62, 219, 128) # tolerance=50
    
    y, i_pixel, j_pixel = get_image_index_and_pixel(point)   
    
##########################################################################################################
         
    pixels_colors = get_pixels_colors(stitched_images[y], (i_pixel, j_pixel), R)       

    b_count = color_counter(pixels_colors, burgundy, tolerance=15)
    r_count = color_counter(pixels_colors, red, tolerance=25)
    y_count = color_counter(pixels_colors, yellow, tolerance=60)
    g_count = color_counter(pixels_colors, green, tolerance=50)
    
##########################################################################################################

    if (b_count + r_count + y_count + g_count) == 0:

        return 'gray'

    elif b_count >= r_count and b_count >= y_count and b_count >= g_count:

        return 'burgundy'

    elif r_count >= b_count and r_count >= y_count and r_count >= g_count:

        return 'red'

    elif y_count >= b_count and y_count >= r_count and y_count >= g_count:

        return 'yellow'

    elif g_count >= b_count and g_count >= r_count and g_count >= y_count:

        return 'green'

    else:

        return 'black'

def get_groing_radius_color(point):
    
    color = 'gray'
    r = None
    
    for r in range(17):
        
        color = get_point_color(point, r)
        if color != 'gray':
            break
            
    return color, r
    
def get_midpoint(point1, point2):
    
    lat1, lon1 = point1
    lat2, lon2 = point2
    
    midpoint = ((lat1 + lat2) / 2, (lon1 + lon2) / 2)
    
    return midpoint

def run_browser(index, y, x):

    global WINDOW_WIDTH, WINDOW_HEIGHT, result_queue
    
    # Initialize Chrome
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--disable-gpu")

    # Set page load strategy to eager
    chrome_options.add_argument("--pageLoadStrategy=eager")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--window-size={},{}".format(WINDOW_WIDTH, WINDOW_HEIGHT))
    driver = webdriver.Chrome(options=chrome_options)

    # Set page load timeout
    driver.set_page_load_timeout(1000)

    # Set script timeout
    driver.set_script_timeout(1000)

    driver.get(f"https://www.google.com/maps/@{y},{x},17z/data=!5m1!1e1?hl=en&entry=ttu")
    # driver.get("https://www.google.com")

    time.sleep(10)  # Wait for 10 seconds to see the result

    # DELETE ALL ELEMENTS
    # try:

    driver.execute_script("arguments[0].remove();", driver.find_element(By.CLASS_NAME, "app-center-widget-holder"))
    driver.execute_script("arguments[0].remove();", driver.find_element(By.CLASS_NAME, "hUbt4d-watermark"))
    driver.execute_script("arguments[0].remove();", driver.find_element(By.ID, "minimap"))

    # Find the elements by class name
    elements_to_delete = driver.find_elements(By.CLASS_NAME, "Hk4XGb")

    # Iterate over the elements and delete each one
    for element_to_delete in elements_to_delete:
        try:
            # if element_to_delete.get_attribute("id") == 'fDahXd':
            #    continue
            driver.execute_script("arguments[0].remove();", element_to_delete)
        except Exception as e:
            continue
    #except:
    #    pass
    
    # Get screenshot as bytes
    screenshot_bytes = driver.get_screenshot_as_png()

    driver.quit()

    # Put the result into the queue with the index `i`
    result_queue.put((index, screenshot_bytes))

    # return screenshot_bytes

def main():
    
    global images
    global stitched_images
    global result_queue
    
    start = datetime.now()

    threads = []
    result_queue = queue.Queue()
    
    current_datetime = datetime.now()

    # Start 24 threads
    for i in range(24):
        thread = threading.Thread(target=run_browser, args=(i, ys[i], x,))
        thread.start()
        time.sleep(21)
        threads.append(thread)

    # Wait for all threads to complete
    for thread in threads:
        thread.join()

    # Collect results from the queue and sort them by the index `i`
    results = [None] * 24
    while not result_queue.empty():
        i, image_bytes = result_queue.get()
        results[i] = image_bytes

    # Convert bytes array to images and save them to a list
    images = [Image.open(io.BytesIO(image_bytes)) for image_bytes in results]

    stitched_images = []
    for y in range(y_range):
        stitched_images.append(glue_images(y, R))

    images = None
    gc.collect()

    ###############################################################################################
    # Parse Segments from Images

    segments = []
    # Total count of points after roads fragmentation and changing geometry to specified column
    for index, item in enumerate(roads.geometry):

        previous_color = None
        previous_point = None
        last_index = None

        for i, point in enumerate(item.coords):

            if is_in_area(point):
                last_index = i
                previous_point = point
                previous_color = get_point_color(point, R)
                break

        if previous_color != None and (last_index + 1) < (len(item.coords) - 1):

            segment = [previous_point]

            for i, current_point in enumerate(item.coords[(last_index + 1):]):

                if is_in_area(current_point):

                    current_color = get_point_color(current_point, R)

                    middle_point = get_midpoint(previous_point, current_point)

                    middle_color = get_point_color(middle_point, R)

                    if current_color != previous_color:

                        if middle_color == previous_color:
                            segment.append(current_point)

                            segments.append({
                                'address_road_id' : roads['id'][index],
                                'color' : previous_color,
                                'geometry' : LineString(segment),
                            })

                            segment = []

                        elif len(segment) > 1:

                            segments.append({
                                'address_road_id' : roads['id'][index],
                                'color' : previous_color,
                                'geometry' : LineString(segment),
                            })

                            segment = [previous_point]

                    segment.append(current_point)
                    previous_point = current_point
                    previous_color = current_color

            if len(segment) > 1:

                segments.append({
                    'address_road_id' : roads.id[index],
                    'color' : previous_color,
                    'geometry' : LineString(segment),
                })

    ###############################################################################################
    # Preprocess data table

    # year   = str(current_datetime.year)
    # month  = str(current_datetime.month) if len(str(current_datetime.month)) == 2 else '0' + str(current_datetime.month)
    # day    = str(current_datetime.day) if len(str(current_datetime.day)) == 2 else '0' + str(current_datetime.day)
    # hour   = str(current_datetime.hour) if len(str(current_datetime.hour)) == 2 else '0' + str(current_datetime.hour)
    # minute = str(current_datetime.minute) if len(str(current_datetime.minute)) == 2 else '0' + str(current_datetime.minute)

    # Convert the list of dictionaries to a GeoDataFrame
    gdf = gpd.GeoDataFrame(segments, geometry='geometry')
    gdf.crs = "EPSG:4326"
    gdf = gdf.to_crs("EPSG:3395")
    gdf['length_meters'] = gdf['geometry'].length
    gdf['date'] = current_datetime.date()
    gdf['time'] = current_datetime.time()
    gdf = gdf.to_crs("EPSG:4326")

    ###############################################################################################
    # Write data table to database

    # Create a connection to the rwh_datalake database
    rwh_datalake_conn = psycopg2.connect(
        dbname='rwh_gis_datalake',
        user='rwh_analytics',
        password='4HPzQt2HyU@',
        host='10.100.200.150',
        port='5439'
    )
    # Create a cursor
    cur = rwh_datalake_conn.cursor()

    for index, row in gdf.iterrows():

        if row['color'] == 'gray':
            continue

        item = [
            row['address_road_id'],
            row['color'],
            row['length_meters'],
            row['date'],
            row['time'],
            dumps(row['geometry']), 
        ]

        query = '''INSERT INTO google_traffic_daily (
                address_road_id, 
                color, 
                length_meters, 
                date, 
                time,
                geometry) 
            VALUES (%s, %s, %s, %s, %s, ST_SetSRID(%s::geometry, 4326))'''

        cur.execute(query, item)

    # Фиксация изменений и закрытие соединения
    rwh_datalake_conn.commit()
    cur.close()
    rwh_datalake_conn.close()

    end = datetime.now()
    print('started:', start)
    print('ended:', end)
    print('total time:', end - start)

def run_schedule():
    # Schedule the job every 30 minutes between 6:00 and 22:00
    for hour in range(6, 23):  # 23 is not inclusive, so this goes up to 22:30
        schedule.every().day.at(f"{hour:02d}:00").do(main)
        schedule.every().day.at(f"{hour:02d}:30").do(main)
        
    # for hour in range(6, 23):  # 23 is not inclusive, so this goes up to 22:30
    #     for minute in range(0, 60, 20):  # 23 is not inclusive, so this goes up to 22:30
    #         schedule.every().day.at(f"{hour:02d}:{minute:02d}").do(main)

    while True:
        schedule.run_pending()
        time.sleep(0.97)


if __name__ == "__main__":
    run_schedule()
